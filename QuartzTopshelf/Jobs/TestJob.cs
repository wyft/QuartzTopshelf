﻿using System;
using System.Collections;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using Quartz;

namespace QuartzTopshelf.Jobs
{
    public sealed class TestJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Called by the <see cref="IScheduler" /> when a <see cref="ITrigger" />
        /// fires that is associated with the <see cref="IJob" />.
        /// </summary>
        /// <remarks>
        /// The implementation may wish to set a  result object on the 
        /// JobExecutionContext before this method exits.  The result itself
        /// is meaningless to Quartz, but may be informative to 
        /// <see cref="IJobListener" />s or 
        /// <see cref="ITriggerListener" />s that are watching the job's 
        /// execution.
        /// </remarks>
        /// <param name="context">The execution context.</param>
        public async Task Execute(IJobExecutionContext context)
        {
            //通过配置文件传递参数
            JobDataMap dataMap = context.JobDetail.JobDataMap;
            string key3 = dataMap.GetString("key3");
            logger.Info("key3 : " + key3);
            string key4 = dataMap.GetString("key4");
            logger.Info("key4 : " + key4);

            logger.Info("TestJob running...");
            //Thread.Sleep(TimeSpan.FromSeconds(5));
            await Console.Out.WriteLineAsync("TestJob is executing.");
            logger.Info("TestJob run finished.");
        }
    }
}
