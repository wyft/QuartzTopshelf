﻿using System.IO;
using System.Reflection;
using Topshelf;

namespace QuartzTopshelf
{
    public static class Program
    {
        static void Main(string[] args)
        {
            // 从服务帐户的目录更改为更符合逻辑的目录
            Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            var logRepository = log4net.LogManager.GetRepository(Assembly.GetEntryAssembly());
            log4net.Config.XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            HostFactory.Run(x => {
                x.RunAsLocalSystem();

                x.SetDescription(Configuration.ServiceDescription);
                x.SetDisplayName(Configuration.ServiceDisplayName);
                x.SetServiceName(Configuration.ServiceName);

                x.Service(factory =>
                {
                    QuartzServer server = QuartzServerFactory.CreateServer();
                    server.Initialize().GetAwaiter().GetResult();
                    return server;
                });
            });
        }
    }
}
