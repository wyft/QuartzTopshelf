﻿using System.Collections.Specialized;
using System.Configuration;

namespace QuartzTopshelf
{
    /// <summary>
    /// 定时服务的配置
    /// </summary>
	public class Configuration
    {
        private const string PrefixServerConfiguration = "quartz.server";
        private const string KeyServiceName = PrefixServerConfiguration + ".serviceName";
        private const string KeyServiceDisplayName = PrefixServerConfiguration + ".serviceDisplayName";
        private const string KeyServiceDescription = PrefixServerConfiguration + ".serviceDescription";
        private const string KeyServerImplementationType = PrefixServerConfiguration + ".type";

        private const string DefaultServiceName = "QuartzServer";
        private const string DefaultServiceDisplayName = "Quartz Server";
        private const string DefaultServiceDescription = "Quartz Job Scheduling Server";
        private static readonly string DefaultServerImplementationType = typeof(QuartzServer).AssemblyQualifiedName;

        private static readonly NameValueCollection configuration;

        /// <summary>
        /// 初始化 Configuration 类
        /// </summary>
		static Configuration()
        {
            configuration = (NameValueCollection)ConfigurationManager.GetSection("quartz");
        }

        /// <summary>
        /// 获取服务的名称
        /// </summary>
        /// <value>服务的名称</value>
		public static string ServiceName
        {
            get { return GetConfigurationOrDefault(KeyServiceName, DefaultServiceName); }
        }

        /// <summary>
        /// 获取服务的显示名称.
        /// </summary>
        /// <value>服务的显示名称</value>
		public static string ServiceDisplayName
        {
            get { return GetConfigurationOrDefault(KeyServiceDisplayName, DefaultServiceDisplayName); }
        }

        /// <summary>
        /// 获取服务描述
        /// </summary>
        /// <value>服务描述</value>
		public static string ServiceDescription
        {
            get { return GetConfigurationOrDefault(KeyServiceDescription, DefaultServiceDescription); }
        }

        /// <summary>
        /// 获取服务器实现的类型名称
        /// </summary>
        /// <value>服务器实现的类型</value>
	    public static string ServerImplementationType
        {
            get { return GetConfigurationOrDefault(KeyServerImplementationType, DefaultServerImplementationType); }
        }

        /// <summary>
        /// 返回具有给定键的配置值。如果的配置不存在，则返回默认值。
        /// </summary>
        /// <param name="configurationKey">用于读取配置的键</param>
        /// <param name="defaultValue">未找到配置时返回的默认值</param>
        /// <returns>配置值</returns>
        private static string GetConfigurationOrDefault(string configurationKey, string defaultValue)
        {
            string retValue = null;
            if (configuration != null)
            {
                retValue = configuration[configurationKey];
            }

            if (retValue == null || retValue.Trim().Length == 0)
            {
                retValue = defaultValue;
            }
            return retValue;
        }
    }
}
