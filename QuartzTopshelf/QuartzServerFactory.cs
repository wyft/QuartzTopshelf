﻿using System;
using System.Reflection;
using log4net;

namespace QuartzTopshelf
{
    /// <summary>
    /// 用于创建Quartz服务实现的工厂类.
    /// </summary>
    public class QuartzServerFactory
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 创建Quartz.NET服务核心的新实例
        /// </summary>
        /// <returns></returns>
        public static QuartzServer CreateServer()
        {
            string typeName = Configuration.ServerImplementationType;

            Type t = Type.GetType(typeName, true);

            //logger.Debug("正在创建服务器类型的新实例'" + typeName + "'");
            QuartzServer retValue = (QuartzServer)Activator.CreateInstance(t);
            //logger.Debug("已成功创建实例");
            return retValue;
        }
    }
}
